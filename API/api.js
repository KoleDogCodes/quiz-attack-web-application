const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

const port = process.env.PORT || 5000;
const CloudManager = require('./manager');

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

app.use(express.static('users'));

app.use(bodyParser.json({ limit: '1000mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '1000mb' }));
app.use(bodyParser.raw({ limit: '1GB' }));

//Connect to the database
mongoose.connect(process.env.MONGO_URL);

//Set email
CloudManager.setEmailDetails(process.env.EMAIL_USERNAME, process.env.EMAIL_PASSWORD);
var badWords = ["4r5e", "5h1t", "5hit", "a55", "anal", "anus", "ar5e", "arrse", "arse", "ass", "ass-fucker", "asses", "assfucker", "assfukka", "asshole", "assholes", "asswhole", "a_s_s", "b!tch", "b00bs", "b17ch", "b1tch", "ballbag", "balls", "ballsack", "bastard", "beastial", "beastiality", "bellend", "bestial", "bestiality", "bi+ch", "biatch", "bitch", "bitcher", "bitchers", "bitches", "bitchin", "bitching", "bloody", "blow job", "blowjob", "blowjobs", "boiolas", "bollock", "bollok", "boner", "boob", "boobs", "booobs", "boooobs", "booooobs", "booooooobs", "breasts", "buceta", "bugger", "bum", "bunny fucker", "butt", "butthole", "buttmuch", "buttplug", "c0ck", "c0cksucker", "carpet muncher", "cawk", "chink", "cipa", "cl1t", "clit", "clitoris", "clits", "cnut", "cock", "cock-sucker", "cockface", "cockhead", "cockmunch", "cockmuncher", "cocks", "cocksuck", "cocksucked", "cocksucker", "cocksucking", "cocksucks", "cocksuka", "cocksukka", "cok", "cokmuncher", "coksucka", "coon", "cox", "crap", "cum", "cummer", "cumming", "cums", "cumshot", "cunilingus", "cunillingus", "cunnilingus", "cunt", "cuntlick", "cuntlicker", "cuntlicking", "cunts", "cyalis", "cyberfuc", "cyberfuck", "cyberfucked", "cyberfucker", "cyberfuckers", "cyberfucking", "d1ck", "damn", "dick", "dickhead", "dildo", "dildos", "dink", "dinks", "dirsa", "dlck", "dog-fucker", "doggin", "dogging", "donkeyribber", "doosh", "duche", "dyke", "ejaculate", "ejaculated", "ejaculates", "ejaculating", "ejaculatings", "ejaculation", "ejakulate", "f u c k", "f u c k e r", "f4nny", "fag", "fagging", "faggitt", "faggot", "faggs", "fagot", "fagots", "fags", "fanny", "fannyflaps", "fannyfucker", "fanyy", "fatass", "fcuk", "fcuker", "fcuking", "feck", "fecker", "felching", "fellate", "fellatio", "fingerfuck", "fingerfucked", "fingerfucker", "fingerfuckers", "fingerfucking", "fingerfucks", "fistfuck", "fistfucked", "fistfucker", "fistfuckers", "fistfucking", "fistfuckings", "fistfucks", "flange", "fook", "fooker", "fuck", "fucka", "fucked", "fucker", "fuckers", "fuckhead", "fuckheads", "fuckin", "fucking", "fuckings", "fuckingshitmotherfucker", "fuckme", "fucks", "fuckwhit", "fuckwit", "fudge packer", "fudgepacker", "fuk", "fuker", "fukker", "fukkin", "fuks", "fukwhit", "fukwit", "fux", "fux0r", "f_u_c_k", "gangbang", "gangbanged", "gangbangs", "gaylord", "gaysex", "goatse", "God", "god-dam", "god-damned", "goddamn", "goddamned", "hardcoresex", "hell", "heshe", "hoar", "hoare", "hoer", "homo", "hore", "horniest", "horny", "hotsex", "jack-off", "jackoff", "jap", "jerk-off", "jism", "jiz", "jizm", "jizz", "kawk", "knob", "knobead", "knobed", "knobend", "knobhead", "knobjocky", "knobjokey", "kock", "kondum", "kondums", "kum", "kummer", "kumming", "kums", "kunilingus", "l3i+ch", "l3itch", "labia", "lust", "lusting", "m0f0", "m0fo", "m45terbate", "ma5terb8", "ma5terbate", "masochist", "master-bate", "masterb8", "masterbat*", "masterbat3", "masterbate", "masterbation", "masterbations", "masturbate", "mo-fo", "mof0", "mofo", "mothafuck", "mothafucka", "mothafuckas", "mothafuckaz", "mothafucked", "mothafucker", "mothafuckers", "mothafuckin", "mothafucking", "mothafuckings", "mothafucks", "mother fucker", "motherfuck", "motherfucked", "motherfucker", "motherfuckers", "motherfuckin", "motherfucking", "motherfuckings", "motherfuckka", "motherfucks", "muff", "mutha", "muthafecker", "muthafuckker", "muther", "mutherfucker", "n1gga", "n1gger", "nazi", "nigg3r", "nigg4h", "nigga", "niggah", "niggas", "niggaz", "nigger", "niggers", "nob", "nob jokey", "nobhead", "nobjocky", "nobjokey", "numbnuts", "nutsack", "orgasim", "orgasims", "orgasm", "orgasms", "p0rn", "pawn", "pecker", "penis", "penisfucker", "phonesex", "phuck", "phuk", "phuked", "phuking", "phukked", "phukking", "phuks", "phuq", "pigfucker", "pimpis", "piss", "pissed", "pisser", "pissers", "pisses", "pissflaps", "pissin", "pissing", "pissoff", "poop", "porn", "porno", "pornography", "pornos", "prick", "pricks", "pron", "pube", "pusse", "pussi", "pussies", "pussy", "pussys", "rectum", "retard", "rimjaw", "rimming", "s hit", "s.o.b.", "sadist", "schlong", "screwing", "scroat", "scrote", "scrotum", "semen", "sex", "sh!+", "sh!t", "sh1t", "shag", "shagger", "shaggin", "shagging", "shemale", "shi+", "shit", "shitdick", "shite", "shited", "shitey", "shitfuck", "shitfull", "shithead", "shiting", "shitings", "shits", "shitted", "shitter", "shitters", "shitting", "shittings", "shitty", "skank", "slut", "sluts", "smegma", "smut", "snatch", "son-of-a-bitch", "spac", "spunk", "s_h_i_t", "t1tt1e5", "t1tties", "teets", "teez", "testical", "testicle", "tit", "titfuck", "tits", "titt", "tittie5", "tittiefucker", "titties", "tittyfuck", "tittywank", "titwank", "tosser", "turd", "tw4t", "twat", "twathead", "twatty", "twunt", "twunter", "v14gra", "v1gra", "vagina", "viagra", "vulva", "w00se", "wang", "wank", "wanker", "wanky", "whoar", "whore", "willies", "willy", "xrated", "xxx"];

app.get('/', (req, res) => {
    return res.json({
        success: true,
        message: 'Connected'
    });
});

//Authenticate User
app.get('/api/account', async(req, res) => {
    const { email, password } = req.query;

    if (await CloudManager.emailExists(email) == false) {
        return res.json({
            success: false,
            message: 'That email address does not exist.'
        });
    }

    var authUser = await CloudManager.authenticateAccount(email, password);

    if (authUser != false) {
        return res.json({
            success: true,
            key: authUser,
            id: await CloudManager.getUserId(authUser),
            message: 'You have successfully logged in.'
        });
    }

    return res.json({
        success: false,
        message: 'Invalid Operation.'
    });

});

//Get User Details
app.get('/api/account/details', async(req, res) => {
    const { key, id } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'That api key does not exist.'
        });
    }

    var data = await CloudManager.getUserDetails(id);

    return res.json({
        success: true,
        message: 'Success',
        data: data
    });

});

//Create Account
app.post('/api/account', async(req, res) => {
    var { email, password, displayName, profileImage } = req.body;

    email = email.trim();
    displayName = displayName.trim();

    //Check password length
    if (password.length < 8) {
        return res.json({
            success: false,
            message: 'Password must be at least 8 characters long'
        });
    }

    //Verify if email is valid
    if (!email.includes("@") && !email.includes(".")) {
        return res.json({
            success: false,
            message: 'Invalid email address'
        });
    }

    //Check for bad words in the displayname
    if (badWords.some(v => displayName.includes(v))) {
        return res.json({
            success: false,
            message: 'Inapporiate displayname.'
        });
    }

    //Verify display name is alphanumeric
    if (displayName.match(/((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i)) {
        return res.json({
            success: false,
            message: "Displaynames cannot contain any spaces or non alphabetic characters."
        });
    }

    if (await CloudManager.emailExists(email)) {
        return res.json({
            success: false,
            message: 'That email address already exists.'
        });
    }

    if (await CloudManager.createAccount(email, password, displayName, profileImage)) {
        return res.json({
            success: true,
            message: 'Please check your email to verify your account.'
        });
    }

    return res.json({
        success: false,
        message: 'Invalid Operation.'
    });

});

//Update User Details
app.put('/api/account', async(req, res) => {
    const { key, profileImage, misc, displayName } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    //Check for bad words in the displayname
    if (badWords.some(v => displayName.includes(v))) {
        return res.json({
            success: false,
            message: 'Inapporiate displayname.'
        });
    }

    //Verify display name is alphanumeric
    if (displayName.match(/((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i)) {
        return res.json({
            success: false,
            message: "Displaynames cannot contain any spaces or non alphabetic characters."
        });
    }

    if (await CloudManager.displayNameExists(displayName)) {
        return res.json({
            success: false,
            message: 'Sorry, that display name is already being used.'
        });
    }

    await CloudManager.updateUserDetails(key, displayName, profileImage, profileImage);

    return res.json({
        success: true,
        message: 'User details successfully updated.'
    });

});

//Get topics
app.get('/api/topics', async(req, res) => {
    const { key } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    return res.json({
        success: true,
        message: 'Topics have been retrieved.',
        topics: await CloudManager.getTopics()
    });
});

//Get posts
app.get('/api/feed', async(req, res) => {
    const { key, topicId } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    return res.json({
        success: true,
        message: 'Posts have been collected',
        posts: await CloudManager.getPosts(topicId)
    });

});

//Create post
app.post('/api/feed', async(req, res) => {
    const { key, topicId, content, image } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    if (content.trim() == "") {
        return res.json({
            success: false,
            message: 'Please type your post cotent inside the text box.'
        });
    }

    await CloudManager.createPost(key, topicId, content, image);

    return res.json({
        success: true,
        message: 'Post has been successfully created.'
    });

});

//Update post
app.put('/api/feed', async(req, res) => {
    const { key, postId, misc } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    if (await CloudManager.updatePost(postId, misc)) {
        return res.json({
            success: true,
            message: 'Post has been updated.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to update post.'
    });

});

//Create quiz
app.post('/api/quiz', async(req, res) => {
    const { key, icon, title, topicId, metadata, status } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    if (await CloudManager.createQuiz(key, icon, title, topicId, metadata, status)) {
        return res.json({
            success: true,
            message: 'Quiz has been created.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create the quiz'
    });

});

//Update Quiz
app.put('/api/quiz', async(req, res) => {
    const { key, id, icon, title, topicId, metadata, status } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    if (await CloudManager.updateQuiz(key, id, icon, title, topicId, metadata, status)) {
        return res.json({
            success: true,
            message: 'Quiz has been updated.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to update the quiz'
    });

});

//Get quiz (TOPIC ID)
app.get('/api/quiz', async(req, res) => {
    const { key, topicId } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    return res.json({
        success: true,
        message: 'Successfully fetched some quizzes.',
        data: await CloudManager.getQuizzes(topicId)
    });

});

//Get quiz (USER's QUIZZES)
app.get('/api/user/quiz', async(req, res) => {
    const { key } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    return res.json({
        success: true,
        message: 'Successfully fetched some quizzes.',
        data: await CloudManager.getCreatedQuizzes(key)
    });

});

//Get quiz (QUIZ ID)
app.get('/api/quiz/:id', async(req, res) => {
    const { key } = req.query;
    const { id } = req.params;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key.'
        });
    }

    return res.json({
        success: true,
        message: 'Successfully fetched the quiz.',
        data: await CloudManager.getQuiz(key, id)
    });

});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});