const mongoose = require('mongoose');

module.exports = mongoose.model('Quiz', new mongoose.Schema({
    owner_id: String,
    icon: String,
    title: String,
    topic_id: String,
    metadata: String,
    status: String,
    created_at: String
}));